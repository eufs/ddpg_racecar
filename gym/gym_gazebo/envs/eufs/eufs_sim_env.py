#!/bin/env/python

"""
This suits as an interface between OpenAI gym and the EUFS simulation
Intended to be using for deep reinforcement learning

Todo:
- parametarise environment launching
- decide on camera image resizing
- decide on observation
- decide on rewards

The MIT License

Copyright (c) 2018-2018 Edinburgh University Formula Student (EUFS)
http://eufs.eusa.ed.ac.uk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

# generic packages
import os
import numpy as np

# ML packages
import gym_gazebo.envs.gazebo_env as gazebo_env
from gym_gazebo.utils.costmap import Costmap
from gym.utils import seeding

# ROS-specific
import rospy
import cv2
from ackermann_msgs.msg import AckermannDriveStamped
from std_srvs.srv import Empty
from sensor_msgs.msg import Image, JointState
from cv_bridge import CvBridge
from nav_msgs.msg import Odometry
import tf


class EUFSGazeboEnv(gazebo_env.GazeboEnv):
    """ EUFSGazeboEnv Class
    Extends the gazebo_env.GazeboEnv class from the OpenAI gym.
    Launches the simulation and all APIs that talk with Gazebo and ROS
    """
    def __init__(self):
        # Launch the simulation with the given launchfile name
        gazebo_env.GazeboEnv.__init__(self, "small_track.launch")

        # Driving command publisher
        self.cmd_pub = rospy.Publisher("/robot_control/command", AckermannDriveStamped, queue_size=5)

        # Services to control Gazebo
        self.unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
        self.reset_proxy = rospy.ServiceProxy('/gazebo/reset_world', Empty)

        # ROS topics to listen to later
        self.camera_topic = "/zed/left/image_rect_color"

        # create costmap filepath
        costmap_name = "gazebo_small_track.npz"
        self.map_filepath = os.path.join(os.path.dirname(os.path.dirname(__file__)), "assets", "maps", costmap_name)

        # TODO: Decide on camera input resizing
        self.rows = 94
        self.cols = 168
        self.gray = False

        # set up debugging screen
        self.display = Costmap(self.map_filepath, border=500, debug=True, window_size=20)

        cw = 1.5 # car_width
        cl = 3.0 # car_length
        self.car_corners = [np.array([-cw/2., cl/2.]), np.array([-cw/2., -cl/2.]),
                            np.array([cw/2., -cl/2.]), np.array([cw / 2., cl / 2.])]

        # TODO
        self.reward_range = (-np.inf, np.inf)

    def set_v_goal(self, speed):
        self.v_goal = speed

    def odom_cb(self, msg):
        """ Depreciated, ignore for the moment
        """
        self.x_pos = msg.pose.pose.position.x
        self.y_pos = msg.pose.pose.position.y
        yaw = None #TODO
        self.display.setPosition(self.x_pos, self.y_pos, yaw)

    def step(self, action):

        # first we must wait for Gazebo to be ready and then unpause the physics
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed: {}".format(e))


        # ACTION
        cmd_v, cmd_theta = action

        # create ROS message to control the car
        msg = AckermannDriveStamped()
        msg.header.stamp = rospy.Time()
        msg.drive.speed = cmd_v
        msg.drive.steering_angle = cmd_theta

        # Becomes True when episode finishes
        done = False

        self.cmd_pub.publish(msg)

        # wait for Odom message and then process it
        odom_msg = rospy.wait_for_message("/ground_truth/state_raw", Odometry)
        self.x_pos = odom_msg.pose.pose.position.x
        self.y_pos = odom_msg.pose.pose.position.y

        # get the yaw
        quat = (odom_msg.pose.pose.orientation.x,
                odom_msg.pose.pose.orientation.y,
                odom_msg.pose.pose.orientation.z,
                odom_msg.pose.pose.orientation.w)

        # get yaw
        _, _, yaw = tf.transformations.euler_from_quaternion(quat)

        # see where we are on the track
        cost = self.display.setPosition(self.x_pos, self.y_pos, yaw)

        # OBSERVATION
        v_current = np.sqrt(odom_msg.twist.twist.linear.x **2 + odom_msg.twist.twist.linear.y **2)
        state = v_current

        # Once we are done, we can pause the physics
        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            self.pause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/pause_physics service call failed: {}".format(e))


        # REWARD
        # reward is the forward speed!
        # get it from the state (have to read in batch dimensions)
        if self.v_goal is not None:
            reward = np.abs(1 / (self.v_goal - v_current))
        else:
            print("Warning goal speed is not set. Please set it!!")
            reward = 0

        return state, reward, done, {} # last arg is info

    def get_state(self):
        image_data = None
        while image_data is None:
            try:
                image_data = rospy.wait_for_message(self.camera_topic, Image, timeout=5)
            except rospy.exceptions.ROSException as e:
                print("ROS exception in image in eufs_sim_env: {}".format(e))

        cv_image = CvBridge().imgmsg_to_cv2(image_data, "bgr8")
        if self.gray: cv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        cv_image = cv_image / 255.0
        cv_image = cv2.resize(cv_image, (self.cols, self.rows)) #(w, h)
        if len(cv_image.shape) == 2: cv_image = np.expand_dims(cv_image, axis=2) # channel dim

        return None

    def reset(self):

        # Resets the state of the environment and returns an initial observation.
        rospy.wait_for_service('/gazebo/reset_world')
        try:
            self.reset_proxy()
        except (rospy.ServiceException) as e:
            print ("/gazebo/reset_simulation service call failed: {}".format(e))

        return None
