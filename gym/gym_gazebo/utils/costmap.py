#!/bin/env/python

"""
Costmap is a program developed for tracking an autonomous racecar in simulation.

Todo:
- somehow automate fetching of costmap images
- rethink how costs should be returned


The MIT License

Copyright (c) 2018-2018 Edinburgh University Formula Student (EUFS)
http://eufs.eusa.ed.ac.uk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import numpy as np
import matplotlib.pyplot as plt
import cv2
import sys
import time


class Costmap():
    """ The Costmap class
    It consists of a costmap grayscale image which shows how is the car
    positioned within the track/road boundaries. Costmaps are in .npz format
    """

    def __init__(self, filepath, border=500, debug=False, window_size=20):
        """Loads a costmap and process all of its data.
        Args:
            filepath    (str)  : absolute path to map file
            border      (int)  : number of pixels to pad the image with
            debug       (bool) : if true, visualises the costmap and the car wihtin it
            window_size (int)  : size of the debugging visualisation window in meters

        Returns:
            None
        """
        
        # load the costmap and save all parameters
        data = np.load(filepath)
        self.xBounds = data["xBounds"]
        self.yBounds = data["yBounds"]
        self.pixelsPerMeter = data["pixelsPerMeter"]

        x_size = int((self.xBounds[1] - self.xBounds[0]) * self.pixelsPerMeter)
        y_size = int((self.yBounds[1] - self.yBounds[0]) * self.pixelsPerMeter)
        self.im_size = [x_size, y_size]

        # get images in numpy format
        self.ch3 = data["channel3"].reshape(self.im_size, order="F").T
        self.ch2 = data["channel2"].reshape(self.im_size, order="F").T
        self.ch1 = data["channel1"].reshape(self.im_size, order="F").T
        self.ch0 = data["channel0"].reshape(self.im_size, order="F").T

        self.border = border
        self.debug = debug

        # pad the image with extra pixels to avoid the car getting off the track
        self.ch0 = cv2.copyMakeBorder(self.ch0 ,self.border,self.border,
                                self.border,self.border,cv2.BORDER_REPLICATE)

        # the size of the square debug window in pixels
        self.window_size = int((window_size * self.pixelsPerMeter - 1) + 1)

        # initiliase position of the car
        self.x_pos = None
        self.y_pos = None
        self.yaw = None

    def getPixelPos(self, x,y):
        """Converts real coordinates to pixel coordinates within the image
        Args:
            x   (int) : x axis coordinates in meters
            y   (int) : y axis coordinates in meters

        Returns:
            x,y (int) : coordinates in pixel
        """
        x = self.ch0.shape[1]/2 + int(x * self.pixelsPerMeter)
        y = self.ch0.shape[0]/2 + int(y * self.pixelsPerMeter)
        return int(x), int(y)

    def setPosition(self, x, y, yaw):
        """Sets the position of yaw of the car within an image and computes if
        the car is within the track.
        Args:
            x   (int)   : x axis coordinates in meters
            y   (int)   : y axis coordinates in meters
            yaw (float) : the heading of the car

        Returns:
            cost (float) : the cost of the location of the car within the track
                           currently considers only the center of mass
                           and returns raw pixel value [0, 1]
                           0 means the car is in the moddile
                           1 or more means the car is ourside the track
                           100 if the car is outside the costmap altogether
        """
        border_m = self.border/self.pixelsPerMeter

        if x < self.xBounds[0] - border_m or x > self.xBounds[1] + border_m or\
           y < self.yBounds[0] - border_m or y > self.yBounds[1] + border_m:
           print("Car is outside the costmap!!")
           return 100

        self.x_pos, self.y_pos = self.getPixelPos(x,y)
        self.yaw = yaw

        # get triangle vertices
        pt1, pt2, pt3 = self.getTrianglePoints(yaw)

        # get triangle vertices in pixels
        # used later for printing a triangle on screen
        self.pt1 = self.getPixelPos(pt1[0] + x, pt1[1] + y)
        self.pt2 = self.getPixelPos(pt2[0] + x, pt2[1] + y)
        self.pt3 = self.getPixelPos(pt3[0] + x, pt3[1] + y)

        if self.debug:
            self.display()

        cost = self.ch0[self.x_pos, self.y_pos]
        return cost

    def getTrianglePoints(self, yaw):
        """Creates triangle vertices on an image to represent the car on the map
        Args:
            yaw (float) : the heading of the car

        Returns:
            pt1, pt2, pt3 (tuple) : the 3 vertices of the triangle
        """
        pt1 = ( 1.5*np.cos(yaw) -      np.sin(yaw),  1.5*np.sin(yaw) +    0*np.cos(yaw))
        pt2 = (-1.5*np.cos(yaw) - -0.6*np.sin(yaw), -1.5*np.sin(yaw) + -0.6*np.cos(yaw))
        pt3 = (-1.5*np.cos(yaw) - +0.6*np.sin(yaw), -1.5*np.sin(yaw) + +0.6*np.cos(yaw))
        return (pt1, pt2, pt3)

    def display(self):
        """Visualises the costmap and the car within it for debugging.
        Note it does a lot of copying and isn't very efficient or fast
        """
        img = self.ch0.copy()

        # draw triangle contour
        triangle_cnt = np.array([self.pt1, self.pt2, self.pt3])
        img = cv2.drawContours(img, [triangle_cnt.astype(int)], -1, 0, 5)

        img = img[int(self.y_pos - self.window_size/2): int(self.y_pos + self.window_size/2),
                  int(self.x_pos - self.window_size/2): int(self.x_pos + self.window_size/2)]

        cv2.imshow("Map", img)
        cv2.waitKey(1)
        # plt.imshow(img, cmap='gray', vmin=0, vmax=1, origin="lower")
        # plt.show()


# for evaluation purposes
if __name__ == '__main__':
    display = Costmap("/home/ignat/ros/mlp_ws/src/gym/gym_gazebo/envs/assets/maps/gazebo_small_track.npz", True, 20)
    try:
        while True:
            x,y, yaw = input("Give me coordinates")
            x = np.array(x, dtype=np.float)
            y = np.array(y, dtype=np.float)
            yaw = np.array(yaw, dtype=np.float)
            print("x=", x)
            print("y=", y)
            print("yaw=", yaw)
            display.setPosition(int(x), int(y), yaw)
        print("Test (hopefully) successful!")

    except KeyboardInterrupt:
        print("Exitting...")
        sys.exit()
    