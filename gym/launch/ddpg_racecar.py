#!/usr/bin/env python

"""Overall launch script for the DDPG training agent

Todo:


The MIT License

Copyright (c) 2018-2018 Edinburgh University Formula Student (EUFS)
http://eufs.eusa.ed.ac.uk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

# generic packages
import os
import time
import sys
import matplotlib.pyplot as plt
import numpy as np
import cv2

# ML packages
import gym
from gym import wrappers
import gym_gazebo

if __name__ == '__main__':

    try:
        env_param = gym.make("EUFSGazeboEnv-v0")
        outdir = '/tmp/gym_experiments'
        env = gym.wrappers.Monitor(env_param, outdir, force=True)
        env_param.set_v_goal(3)
        env.reset()

        while True:
            env.step((1.,0.3))
            # time.sleep(0.2)

    except KeyboardInterrupt:
        print("Exiting...")
        cv2.destroyAllWindows()
        env.close()
        sys.exit()
