# ddpg_racecar

Making a racecar drive itself with deep reinforcement learning


## Setup
Basic requirements:
- Ubuntu 16.04
- [ROS Kinetic desktop-full](http://wiki.ros.org/kinetic/Installation)
- [EUFS Simulation - ddpg_racecar branch](https://github.com/eufsa/eufs_sim/tree/ddpg_racecar)
- Gazebo (Tested with ver 7.0 and 10.0 but anything 7+ should work)
- Python 3 (tested with 3.5.2)
- [Catkin tools](https://catkin-tools.readthedocs.io/en/latest/installing.html)
- [OpenAI gym](https://github.com/openai/gym)


## Create a catkin workspace
```bash
mkdir mlp_ws
cd mlp_ws
mkdir src
cd src
git clone https://github.com/ros/geometry
git clone https://github.com/ros/geometry2
```

Now put this repository in your `mlp_ws/src/` folder

## Create a python virtual environemnt
```bash
virtualenv -p /usr/bin/python3 mlp
source venv/bin/activate
pip install rospkg  h5py keras shapely netifaces defusedxml catkin_pkg pyyaml empy  numpy opencv-python matplotlib
```

(using the same environment)

## Install Gazebo gym
Go to the `gym` folder of this repository
```bash
pip install -e .
```


## Build and run
```bash
cd mlp_ws
catkin_make
source devel/setup.bash
python src/gym/launch/ddpkg_racecar.py
```

##  Potential issues

### ROS OpenCV
When you source ROS, it creates it's own instance of openCV which can't be used
with Python3. If you are encoutering some issue like "can't import cv2" then
you'll have to remove the link to ROS's OpenCV. Note that this breaks OpenCV
in ROS

```bash
cd /opt/ros/kinetic/lib/python2.7/dist-packages/
mv cv2.so cv2-ros.so
```